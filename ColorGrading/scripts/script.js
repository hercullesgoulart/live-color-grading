const Materials = require('Materials');
const Scene = require('Scene');
const NativeUI = require('NativeUI');
const Textures = require('Textures');
const Patches = require('Patches');
const TouchGestures = require('TouchGestures');
const Diagnostics = require('Diagnostics');
const Time = require('Time');
const Persistence = require('Persistence');
const CameraInfo = require('CameraInfo');

 const dataText = Scene.root.find('text0');


const index = 0;


const data = { 
               rH: 0, rM: 0, rL:0, 
               gH: 0, gM: 0, gL:0,
               bH: 0, bM: 0, bL:0
             };
const userScope = Persistence.userScope;
// Create a JavaScript object to store the data

 

 

const configuration = {
  selectedIndex: index,
  items: [
    { image_texture: Textures.get('R1') },
    { image_texture: Textures.get('R2') },
    { image_texture: Textures.get('R3') },
    { image_texture: Textures.get('R4') },
    { image_texture: Textures.get('R5') },
    { image_texture: Textures.get('R6') },
    { image_texture: Textures.get('R7') },
    { image_texture: Textures.get('R8') },
    { image_texture: Textures.get('R9') },
  ],
};
var indexnumber = 0;

 


const picker = NativeUI.picker;
picker.configure(configuration);

 

picker.visible = true;

 

// Slider
var lastSliderValue = 0.5;
const nativeUISlider = NativeUI.slider;

 


picker.selectedIndex.monitor().subscribe(function(val) {
  Patches.setScalarValue("Number", val.newValue);
  indexnumber = val.newValue;
  //Diagnostics.log(indexnumber);
  
});

function save(){
  Diagnostics.log("salvando");
  userScope.set('data',data);
  Diagnostics.log("salvou");
};


userScope.get('data').then(function(result) {

  // Output a success message with the data added
//  dataText.text = 'Successfully retrieved data ' + result.rH;

  Patches.setScalarValue("number1", result.rH);  
  Patches.setScalarValue("number2", result.gH);  
  Patches.setScalarValue("number3", result.bH);  
  Patches.setScalarValue("number4", result.rM);  
  Patches.setScalarValue("number5", result.gM);  
  Patches.setScalarValue("number6", result.bM);  
  Patches.setScalarValue("number7", result.rL);  
  Patches.setScalarValue("number8", result.gL);  
  Patches.setScalarValue("number9", result.bL);  
  
 
  // Output a success message with the data added
  //dataText.text = 'Successfully retrieved data ' + result.rH;
  data.rH = result.rH;
  data.rM = result.rM;
  data.rL = result.rL;

 

  data.gH = result.gH;
  data.gM = result.gM;
  data.gL = result.gL;

 

  data.bH = result.bH;
  data.bM = result.bM;
  data.bL = result.bL;

  
}).catch(function(error) {

    // Output a failure message with the error returned
    //dataText.text = 'Failed to load, ' + error;
    configureSlider();
  });


nativeUISlider.value.monitor({fireOnInitialValue: false}).subscribe(function(val) {
    lastSliderValue = val.newValue;
    
    let sliderData = { 'sliderVal': lastSliderValue };

    if ( indexnumber == 0) {
      Patches.setScalarValue("number1", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.rH = lastSliderValue;
     
    }else if ( indexnumber == 1) {
      Patches.setScalarValue("number2", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch      
      data.gH = lastSliderValue;
  
    }else  if ( indexnumber == 2) {
      Patches.setScalarValue("number3", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.bH = lastSliderValue;

    }else if ( indexnumber == 3) {
      Patches.setScalarValue("number4", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.rM = lastSliderValue;
  
    }else  if ( indexnumber == 4) {
      Patches.setScalarValue("number5", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.gM = lastSliderValue;
  
    }else if ( indexnumber == 5) {
      Patches.setScalarValue("number6", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.bM = lastSliderValue;
  
    }else  if ( indexnumber == 6) {
      Patches.setScalarValue("number7", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.rL = lastSliderValue;
  
    }else if ( indexnumber == 7) {
      Patches.setScalarValue("number8", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.gL = lastSliderValue;
  
    }else  if ( indexnumber == 8) {
      Patches.setScalarValue("number9", ((lastSliderValue - 0.5) * 0.8));//set the value to the patch
      data.bL = lastSliderValue;
      
    }
  });


 
 // Store references to the photo capture and video recording boolean signals
//const isCapturingPhoto = CameraInfo.isCapturingPhoto; not working
const isRecordingVideo = CameraInfo.isRecordingVideo;


isRecordingVideo.monitor().subscribe(function(){
  save();
});

  
  function configureSlider(){
      nativeUISlider.value = lastSliderValue;
  };
  
  function init()//set the initial value and active slider visibility 
  {
    nativeUISlider.visible = true;
  };
  
  init();
